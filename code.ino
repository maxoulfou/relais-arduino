int etat_button;
const int relay = 13;

void setup() {
  pinMode(relay, OUTPUT);
  pinMode(etat_button, INPUT);
}

void loop() {
  etat_button = digitalRead(0);
  if (etat_button == 1) {
    digitalWrite(relay, HIGH);
  }
  else {
    digitalWrite(relay, LOW);
  }
}


