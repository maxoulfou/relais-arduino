# TP Relais Arduino

- [x] Codes sources Arduino (.ino)
- [x] Fichiers sources schéma électrique (Proteus)
---
- [ ] Codes Arduino commentés
- Pour faire fonctionner le relais il suffit de reprendre la trame des codes pour faire fonctionner une led.

<img src = "https://gitlab.com/maxoulfou/relais-arduino/raw/master/Images/Sch%C3%A9ma_circuit.PNG" title = "schéma" alt = "Relais Arduino Schema">

© Maxence Brochier 2018 - 2019